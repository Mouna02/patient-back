from django.db import models

class Patient(models.Model):
    id_patient = models.IntegerField()
    email = models.EmailField()
    birthday = models.DateField()

    def __str__(self):
        return '%s by %s' % (self.email, self.birthday)